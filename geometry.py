#!/usr/bin/env python3
#
# TODO
#
# - Handle infinity, nan and division by zero.
# - Shorten 3D triangle trace method.
#

"""

Geometric operations

This module contains geometric operations for digital 2D line
segments and real 2D lines, 3D lines and 3D triangles.

Note that division by zero raises an error and operations with very
small or large numbers yield zero, infinity or nan.

"""

class DigitalSegment2D:
	@staticmethod
	def points(p1, p2):
		"""
		Given two integer 2D points, get a generator of the points on a
		line segment between those two points. See:
		
		 https://en.wikipedia.org/wiki/Bresenham's_line_algorithm
		"""
		def f(p1, p2):
			if p2[0] > p1[0]:
				xi = 1
				dx = p2[0] - p1[0]
			else:
				xi = -1
				dx = p1[0] - p2[0]

			if p2[1] > p1[1]:
				yi = 1
				dy = p2[1] - p1[1]
			else:
				yi = -1
				dy = p1[1] - p2[1]
			
			D = 2 * dy - dx
			y = p1[1]

			for x in range(p1[0], p2[0] + xi, xi):
				yield (x, y)
				
				if D > 0:
					y += yi
					D -= 2 * dx

				D += 2 * dy

		if abs(p2[0] - p1[0]) > abs(p2[1] - p1[1]):
				return ((x, y) for x, y in f((p1[0], p1[1]), (p2[0], p2[1])))
		else:
				return ((x, y) for y, x in f((p1[1], p1[0]), (p2[1], p2[0])))

class Line2D:
	@staticmethod
	def slope(p1, p2):
		"""
		Given two real 2D points, get the slope of the line with those two
		points.
		"""
		return (p2[1] - p1[1]) / (p2[0] - p1[0])
	
	@staticmethod
	def y_intercept(p1, p2, x = 0):
		"""
		Given two real 2D points and a x-coordinate defaulting to zero,
		get the intersection of the line with those two points and the
		y-axis with that x-coordinate.
		"""
		return p1[1] - (p2[1] - p1[1]) / (p2[0] - p1[0]) * (p1[0] - x)
	
	@staticmethod
	def x_intercept(p1, p2, y = 0):
		"""
		Given two real 2D points and a y-coordinate defaulting to zero,
		get the intersection of the line with those two points and the
		x-axis with that y-coordinate.
		"""
		return p2[0] - (p2[0] - p1[0]) / (p2[1] - p1[1]) * (p2[1] - y)

class Line3D:
	@staticmethod
	def xy_plane_intersection(p1, p2, z = 0):
		"""
		Given two real 3D points and a z-coordinate defaulting to zero,
		get the intersection of the line with those two points and the
		xy-plane with that z-coordinate.
		"""
		x = Line2D.x_intercept((p1[0], p1[2]), (p2[0], p2[2]), z)
		y = Line2D.x_intercept((p1[1], p1[2]), (p2[1], p2[2]), z)
		return (x, y)

class Triangle3D:
	@staticmethod
	def xy_plane_intersection(p1, p2, p3, z = 0):
		"""
		Given three real 3D points and a z-coordinate defaulting to zero,
		get the intersection of the triangle between those three points
		and the xy-plane with that z-coordinate.
		"""
		z1 = p1[2]
		z2 = p2[2]
		z3 = p3[2]

		if z1 < z:
			if z2 < z:
				if z3 < z:
					# All vertices below the plane.
					return ()
				else:
					# Two vertices below the plane.
					return ((
						Line3D.xy_plane_intersection(p1, p3, z),
						Line3D.xy_plane_intersection(p2, p3, z)),)
			else:
				if z3 < z:
					# Two vertices below the plane.
					return ((
						Line3D.xy_plane_intersection(p1, p2, z),
						Line3D.xy_plane_intersection(p3, p2, z)),)
				else:
					# One vertex below the plane.
					return ((
						Line3D.xy_plane_intersection(p1, p2, z),
						Line3D.xy_plane_intersection(p1, p3, z)),)
		else:
			if z2 < z:
				if z3 < z:
					# Two vertices below the plane.
					return ((
						Line3D.xy_plane_intersection(p2, p1, z),
						Line3D.xy_plane_intersection(p3, p1, z)),)
				else:
					# One vertex below the plane.
					return ((
						Line3D.xy_plane_intersection(p2, p1, z),
						Line3D.xy_plane_intersection(p2, p3, z)),)
			else:
				if z3 < z:
					# One vertex below the plane.
					return ((
						Line3D.xy_plane_intersection(p3, p1, z),
						Line3D.xy_plane_intersection(p3, p2, z)),)
				else:
					# No vertex below the plane.
					if z1 == z:
						if z2 == z:
							if z3 == z:
								# All vertices in the plane.
								return (
									Line3D.xy_plane_intersection(p1, p2, z),
									Line3D.xy_plane_intersection(p2, p3, z),
									Line3D.xy_plane_intersection(p3, p1, z))
							else:
								# Two vertices in the plane.
								return ((
									Line3D.xy_plane_intersection(p1, p3, z),
									Line3D.xy_plane_intersection(p2, p3, z)),)
						else:
							if z3 == z:
								# Two vertices in the plane.
								return ((
									Line3D.xy_plane_intersection(p1, p2, z),
									Line3D.xy_plane_intersection(p3, p2, z)),)
							else:
								# One vertex in the plane.
								return ((
									Line3D.xy_plane_intersection(p1, p2, z),
									Line3D.xy_plane_intersection(p1, p3, z)),)
					else:
						if z2 == z:
							if z3 == z:
								# Two vertices in the plane.
								return ((
									Line3D.xy_plane_intersection(p2, p1, z),
									Line3D.xy_plane_intersection(p3, p1, z)),)
							else:
								# One vertex in the plane.
								return ((
									Line3D.xy_plane_intersection(p2, p1, z),
									Line3D.xy_plane_intersection(p2, p3, z)),)
						else:
							if z3 == z:
								# One vertex in the plane.
								return ((
									Line3D.xy_plane_intersection(p3, p1, z),
									Line3D.xy_plane_intersection(p3, p2, z)),)
							else:
								# No vertex in the plane.
								return ()

