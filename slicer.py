from time import process_time

class Processor:
	def preprocess_slices(self, F, Z):
		...
	
	def preprocess_slice(self, i):
		...
	
	def process_line(self, i, line):
		...
	
	def postprocess_slice(self, i):
		...
	
	def postprocess_slices(self):
		...

class Slicer:
	@staticmethod
	def slice(mesh, F, Z, processors):
		def process(method_name, * arguments):
			t = process_time()
			for processor in processors:
				getattr(processor, method_name)(* arguments)
			return process_time() - t

		slices = mesh.get_slices(Z, F)
		process('preprocess_slices', F, Z)
		
		for i, slice in enumerate(slices):
			ts = process_time()
			tsp = process('preprocess_slice', i)

			for face_lines in slice:
				for j, line in enumerate(face_lines):
					tsp += process('process_line', j, line)
			
			tsp += process('postprocess_slice', i)
			ts = process_time() - ts - tsp
			print(f'time={ts:.3f},{tsp:.3f}s')

		process('postprocess_slices')

