#!/usr/bin/env python3

"""

3D triangle mesh

This module contains classes for a 3D triangle mesh and its vertices
and faces.

"""

from functools import wraps
from math import inf
from bisect import bisect_right
from geometry import Triangle3D

class Vertices:
	def __init__(self):
		"""
		Create a list of vertices.
		"""
		self._points = [None]
		self._minimum = [ inf,  inf,  inf]
		self._maximum = [-inf, -inf, -inf]

	def _update_minimum(self, x, y, z):
		if x < self._minimum[0]: self._minimum[0] = x
		if y < self._minimum[1]: self._minimum[1] = y
		if z < self._minimum[2]: self._minimum[2] = z
	
	def _update_maximum(self, x, y, z):
		if x > self._maximum[0]: self._maximum[0] = x
		if y > self._maximum[1]: self._maximum[1] = y
		if z > self._maximum[2]: self._maximum[2] = z

	def append(self, x, y, z):
		"""
		Given xyz-coordinates, append to to this list a vertex at those
		coordinates.
		"""
		self._points.append([x, y, z])
		self._update_minimum(x, y, z)
		self._update_maximum(x, y, z)

	@property
	def size(self):
		"""
		Get the number of vertices in this list.
		"""
		return len(self._points) - 1

	@property
	def left(self):
		"""
		Get the minimum x-coordinate of the vertices in this list.
		"""
		return self._minimum[0]
	
	@property
	def back(self):
		"""
		Get the minimum y-coordinate of the vertices in this list.
		"""
		return self._minimum[1]
	
	@property
	def bottom(self):
		"""
		Get the minimum z-coordinate of the vertices in this list.
		"""
		return self._minimum[2]
	
	@property
	def right(self):
		"""
		Get the maximum x-coordinate of the vertices in this list.
		"""
		return self._maximum[0]
	
	@property
	def front(self):
		"""
		Get the maximum y-coordinate of the vertices in this list.
		"""
		return self._maximum[1]
	
	@property
	def top(self):
		"""
		Get the maximum z-coordinate of the vertices in this list.
		"""
		return self._maximum[2]

	@property
	def width(self):
		"""
		Get the difference of the maximum and the minimum x-coordinates of
		the vertices in this list.
		"""
		return self.right - self.left
	
	@property
	def depth(self):
		"""
		Get the difference of the maximum and the minimum y-coordinates of
		the vertices in this list.
		"""
		return self.front - self.back

	@property
	def height(self):
		"""
		Get the difference of the maximum and the minimum z-coordinates of
		the vertices in this list.
		"""
		return self.top - self.bottom
	
	def get(self, index):
		"""
		Given the index of a vertex in this list, get the coordinates of
		that vertex. Indices are non-zero integers such that its absolute
		value is not greater than the size of this list.
		"""
		return self._points[index].copy()

class Faces:
	def __init__(self):
		"""
		Create a list of faces.
		"""
		self._faces = [None]

	def append(self, i, j, k):
		"""
		Given the indices of three vertices, append to this list a face
		with those vertices.
		"""
		self._faces.append([i, j, k])

	@property
	def size(self):
		"""
		Get the number of faces in this list.
		"""
		return len(self._faces) - 1

	def get(self, index):
		"""
		Given the index of a face in this list, get the indices of the
		vertices of that face.
		"""
		return self._faces[index].copy()

class VertexValueError(Exception):
	def __init__(self, line_number):
		message = (
			f'vertex definition at line {line_number} '
			f'expects three or more numbers')
		super().__init__(message)
		self.line_number = line_number

class FaceValueError(Exception):
	def __init__(self, line_number):
		message = (
			f'face definition at line {line_number} expects '
			f'non-zero integers')
		super().__init__(message)
		self.line_number = line_number

class Mesh:
	@staticmethod
	def read(file):
		"""
		Given the path of an OBJ file, create a triangular mesh with the
		vertices and faces defined in that file. Note that:

		 - Vertices with two or fewer coordinates raise VertexValueError.
		   Vertices with three or more coordinates are read until the
		   third coordinate and raise VertexValueError if any of them is
		   not a number.

		 - Faces with zero vertices are ignored; faces with one, two and
		   three vertices are read as points, lines and triangles. Faces
		   with more than three vertices are read as triangle fans
		   centered at the first vertex. Non-zero vertex indices raise
		   FaceValueError.
		"""
		mesh = Mesh()
		
		def read_vertex(line_number, line):
			try:
				x = float(line[1])
				y = float(line[2])
				z = float(line[3])
			except ValueError:
				raise VertexValueError(line_number)

			mesh.vertices.append(x, y, z)
		
		def read_face(line_number, line):
			n = len(line)
			
			try:
				indices = [int(line[i].split('/')[0]) for i in range(1, n)]
			except ValueError:
				raise FaceValueError(line_number)

			if n > 1:
				mesh.faces.append(
					indices[0],
					indices[1] if n > 2 else indices[0],
					indices[2] if n > 3 else indices[1])
				
				for i in range(3, n - 1):
					mesh.faces.append(indices[i], indices[0], indices[i-1])

		lines = file.readlines()
		
		for line_index, line in enumerate(lines):
			line = line.strip()
			line = line.split()
			
			if len(line) == 0:
				continue
			elif line[0] == 'v':
				read_vertex(line_index + 1, line)
			elif line[0] == 'f':
				read_face(line_index + 1, line)

		return mesh

	def __init__(self):
		"""
		Create a triangular mesh.
		"""
		self._vertices = Vertices()
		self._faces = Faces()

	# Get the vertices of this mesh.
	@property
	def vertices(self):
		"""
		Get the vertices in this mesh.
		"""
		return self._vertices
	
	# Get the faces of this mesh.
	@property
	def faces(self):
		"""
		Get the faces in this mesh.
		"""
		return self._faces

	def _from_face_index_to_vertices(f):
		@wraps(f)
		def function(self, index, * arguments, ** keyword_arguments):
			i, j, k = self._faces.get(index)
		
			u = self._vertices.get(i)
			v = self._vertices.get(j)
			w = self._vertices.get(k)
			
			return f(u, v, w, * arguments, ** keyword_arguments)
		
		return function

	@_from_face_index_to_vertices
	def get_face_left(u, v, w):
		"""
		Given the index of a face in this mesh, get the minimum
		x-coordinate of the vertices of that face.
		"""
		return min(u[0], v[0], w[0])
	
	@_from_face_index_to_vertices
	def get_face_back(u, v, w):
		"""
		Given the index of a face in this mesh, get the minimum
		y-coordinate of the vertices of that face.
		"""
		return min(u[1], v[1], w[1])
	
	@_from_face_index_to_vertices
	def get_face_bottom(u, v, w):
		"""
		Given the index of a face in this mesh, get the minimum
		z-coordinate of the vertices of that face.
		"""
		return min(u[2], v[2], w[2])
	
	@_from_face_index_to_vertices
	def get_face_right(u, v, w):
		"""
		Given the index of a face in this mesh, get the maximum
		x-coordinate of the vertices of that face.
		"""
		return max(u[0], v[0], w[0])

	@_from_face_index_to_vertices
	def get_face_front(u, v, w):
		"""
		Given the index of a face in this mesh, get the maximum
		y-coordinate of the vertices of that face.
		"""
		return max(u[1], v[1], w[1])

	@_from_face_index_to_vertices
	def get_face_top(u, v, w):
		"""
		Given the index of a face in this mesh, get the maximum
		z-coordinate of the vertices of that face.
		"""
		return max(u[2], v[2], w[2])

	@_from_face_index_to_vertices
	def get_face_slice(u, v, w, z):
		"""
		Given the index of a face in this mesh and a z-coordinate, get the
		intersection of that face with the xy-plane at that coordinate,
		a tuple with one or three tuples representing lines.
		"""
		return Triangle3D.xy_plane_intersection(u, v, w, z)

	def get_faces_by_bounding_xy_planes(self, Z):
		"""
		Given a collection of z-coordinates, get a function f such that
		f(i, j) is the collection of the indices of the faces in this mesh
		such that for every face:
		
		           bottom(face) < Z[i], i = 0, len(Z) > 0
		 Z[i-1] <= bottom(face) < Z[i], 0 < i < len(Z)
		 Z[i-1] <= bottom(face),        i = len(Z), len(Z) > 0
		 
		           top(face) < Z[0],    j = 0, len(Z) > 0
		 Z[j-1] <= top(face) < Z[j],    0 < j < len(Z)
		 Z[j-1] <= top(face),           j = len(Z), len(Z) > 0
		"""
		Z = sorted(Z)
		n = len(Z)
		indices = [[[] for j in range(n + 1)] for i in range(n + 1)]
		
		def slice_index(z):
			# TODO Test thoroughly.
			# return max(0, min(n, int((z - Z[0]) // (Z[1] - Z[0]))))
			return bisect_right(Z, z)
		
		for index in range(1, self.faces.size + 1):
			i = slice_index(self.get_face_bottom(index))
			j = slice_index(self.get_face_top(index))
			indices[i][j].append(index)
		
		def f(i, j):
			for index in indices[i][j]:
				yield index
		
		return f
	
	def get_faces_by_possibly_intersecting_xy_plane(self, Z):
		"""
		Given a collection of z-coordinates, get a function f such that
		f(i) is the collection of the indices of the faces in this mesh
		such that for every face:
		
		 bottom(face) < Z[i+1], Z[i] <= top(face), 0 <= i < len(Z)
		"""
		n = len(Z)
		indices = self.get_faces_by_bounding_xy_planes(Z)
		
		def f(slice_index):
			for i in range(0, slice_index + 2):
				for j in range(slice_index + 1, n + 1):
					for index in indices(i, j):
						yield index
		
		return f
	
	def get_slice(self, z, indices):
		"""
		Given a collection of indices of faces in this mesh and a
		z-coordinate, get a generator of the intersection of each of those
		faces with the xy-plane at that coordinate. See
		Mesh.get_face_slice.
		"""
		for index in indices:
			yield self.get_face_slice(index, z)
	
	def get_slices(self, Z, indices):
		"""
		Given a collection of indices of faces in this mesh and a 
		collection of z-coordinates, get a generator of the intersection
		of each of those faces with the xy-plane at each of those
		coordinates. See Mesh.get_slice.
		"""
		for i, z in enumerate(Z):
			yield self.get_slice(z, indices(i))

