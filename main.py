#!/usr/bin/env python3

# TODO Slice duration dependant on distance to next slice.
# TODO Comunication between processors.
# TODO Non-static slicer (so slicer object can be used by processors).

from sys import stdin
from time import process_time
from mesh import Mesh
from processor import *
from slicer import Slicer

loading_start_time = process_time()
mesh = Mesh.read(stdin)
loading_stop_time = process_time()
loading_run_time = loading_stop_time - loading_start_time
print(f'Loading run time: {loading_run_time}s')

w = 1000
dx = mesh.vertices.width / w
x0 = mesh.vertices.left

d = 1000
dy = mesh.vertices.depth / d
y0 = mesh.vertices.back

h = 100
dz = mesh.vertices.height / h
z0 = mesh.vertices.bottom + dz / 2
Z = [dz * i + z0 for i in range(h)]

processors = (
	StatisticsProcessor(),
	SVGProcessor('output.svg', w, d, x0, y0, dx, dy),
	GIFProcessor('output.gif', w, d, x0, y0, dx, dy),
)

face_indexing_start_time = process_time()
F = mesh.get_faces_by_possibly_intersecting_xy_plane(Z)
face_indexing_stop_time = process_time()
face_indexing_run_time = face_indexing_stop_time - face_indexing_start_time
print('Face indexing run time:', face_indexing_run_time)

slicing_start_time = process_time()
Slicer.slice(mesh, F, Z, processors)
slicing_stop_time = process_time()
slicing_run_time = slicing_stop_time - slicing_start_time
print(f'Slicing run time: {slicing_run_time}s')

