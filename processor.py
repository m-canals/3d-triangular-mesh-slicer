#!/usr/bin/env python3

from slicer import Processor

class SVGProcessor(Processor):
	def __init__(self, path, width, height, x0, y0, dx, dy, duration = 100):
		self.path = path
		self.width = width
		self.height = height
		self.duration = duration
		self.f = lambda p: (
			(p[0] - x0) / dx,
			self.height - 1 - (p[1] - y0) / dy)
	
	def preprocess_slices(self, F, Z):
		self.depth = len(Z)
		self.file = open('output.svg', 'w')
		self.file.write(
			f'<?xml version="1.0"?>\n'
			f'<svg xmlns="http://www.w3.org/2000/svg" version="1.1" '
			f'viewBox="0 0 {self.width} {self.height}">\n'
			f'<style>line {{stroke: black; stroke-width: 1px;}}</style>\n')

	def preprocess_slice(self, i):
		self.file.write(
			f'<g visibility="hidden">\n'
			f'<set id="a{i}" attributeName="visibility" to="visible" '
			f'begin="{"0s;" if i == 0 else ""}a{(i - 1) % self.depth}.end" '
			f'dur="{self.duration}ms"/>\n'
			f'<set id="b{i}" attributeName="visibility" to="hidden" '
			f'begin="a{i}.end"/>\n')

	def process_line(self, i, line):
		(x1, y1), (x2, y2) = self.f(line[0]), self.f(line[1])
		self.file.write(
			f'<line x1="{x1:.2f}" y1="{y1:.2f}" x2="{x2:.2f}" y2="{y2:.2f}"/>\n')

	def postprocess_slice(self, i):
		self.file.write('</g>\n')
	
	def postprocess_slices(self):
		self.file.write('</svg>')
		self.file.close()

from PIL import Image
from geometry import DigitalSegment2D

class GIFProcessor(Processor):
	def __init__(self, path, width, height, x0, y0, dx, dy):
		self.path = path
		self.width = width
		self.height = height
		self.size = (width, height)
		self.void_color = (0, 0, 0)
		self.line_color = (255, 255, 255)
		self.f = lambda p: (
			max(0, min(self.width - 1, round((p[0] - x0) / dx))),
			max(0, min(self.height - 1, round((p[1] - y0) / dy))))

	def preprocess_slices(self, F, Z):
		self.depth = len(Z)
		self.images = []

	def preprocess_slice(self, i):
		self.image = Image.new('RGB', self.size, self.void_color)

	def process_line(self, i, line):
		p1, p2 = self.f(line[0]), self.f(line[1])
		for x, y in DigitalSegment2D.points(p1, p2):
			self.image.putpixel((x, self.height - 1 - y), self.line_color)

	def postprocess_slice(self, i):
		self.images.append(self.image)

	def postprocess_slices(self):
		if len(self.images) > 0:
			self.images[0].save(self.path,
				save_all = True,
				append_images = self.images[1:],
				optimize = False,
				duration = 100,
				loop = 0)

class StatisticsProcessor(Processor):
	def __init__(self):
		pass

	def preprocess_slices(self, F, Z):
		self.tnl = 0
		self.F = F
		self.Z = Z

	def preprocess_slice(self, i):
		self.nf = len(list(self.F(i)))
		self.nif = 0
		self.nl = 0

	def process_line(self, i, line):
		self.nl += 1
		self.nif += 1 if i == 0 else 0

	def postprocess_slice(self, i):
		self.tnl += self.nl
		self.cpf = (self.nl - self.nif) // 2
	
		print(
			f'#{i} z={self.Z[i]} faces={self.nf} int-faces={self.nif} lines={self.nl} '
			f'\033[{41 * (self.cpf > 0)}mcp-faces={self.cpf}\033[0m ', sep = '')

	def postprocess_slices(self):
		print(f'Generated lines: {self.tnl}')

